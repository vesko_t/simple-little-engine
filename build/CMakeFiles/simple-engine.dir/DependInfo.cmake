# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vesko/projects/simple-little-engine/src/collision/OrientedBoundingBox.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/collision/OrientedBoundingBox.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/io/input.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/io/input.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/io/window.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/io/window.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/main.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/main.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/model/SimpleComponent.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/model/SimpleComponent.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/model/SimpleObject.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/model/SimpleObject.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/model/Transform.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/model/Transform.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/rendering/Rectangle.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/rendering/Rectangle.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/rendering/Shader.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/rendering/Shader.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/rendering/SpriteRenderer.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/rendering/SpriteRenderer.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/rendering/Texture.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/rendering/Texture.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/util/shaders.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/util/shaders.cpp.o"
  "/home/vesko/projects/simple-little-engine/src/util/time.cpp" "/home/vesko/projects/simple-little-engine/build/CMakeFiles/simple-engine.dir/src/util/time.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../glm/glm/.."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
