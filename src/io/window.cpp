#include "window.hpp"

namespace sle
{

GLFWwindow *windowRef;

int createWindow(std::string title, int width, int height, bool vsync, bool fullscreen)
{
    if (!glfwInit())
    {
        glfwTerminate();
        return -1;
    }

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();

    const GLFWvidmode *vidMode = glfwGetVideoMode(monitor);

    windowRef = glfwCreateWindow(fullscreen ? vidMode->width : width,
                                 fullscreen ? vidMode->height : height,
                                 title.c_str(), fullscreen ? monitor : 0, 0);

    if (!windowRef)
    {
        glfwTerminate();
        return -1;
    }

    if (!fullscreen)
    {
        glfwSetWindowPos(windowRef, (vidMode->width - width) / 2, (vidMode->height - height) / 2);
    }

    if (vsync)
        glfwSwapInterval(1);

    glfwMakeContextCurrent(windowRef);
    glewInit();
}
void windowAfterRender()
{
    glfwSwapBuffers(windowRef);
    glfwPollEvents();
    glClear(GL_COLOR_BUFFER_BIT);
}
bool windowShouldClose()
{
    return glfwWindowShouldClose(windowRef) == 1;
}
void destroyWindow()
{
    glfwDestroyWindow(windowRef);
    glfwTerminate();
}

void setVsync(bool vsync){
    if (vsync)
        glfwSwapInterval(1);

}

} // namespace sle