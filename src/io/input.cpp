#include "input.hpp"

bool keys[GLFW_KEY_LAST];
bool buttons[3];

namespace sle
{
void init(void)
{
    for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++)
    {
        keys[i] = false;
    }
    for (int i = 0; i < 3; i++)
    {
        buttons[i] = false;
    }
}

void inputUpdate(void)
{
    for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++)
    {
        keys[i] = isKeyDown(i);
    }
    for (int i = 0; i < 3; i++)
    {
        buttons[i] = isMouseButtonDown(i);
    }
}

glm::vec2 getMousePositionScreen();

bool isKeyDown(int key)
{
    return glfwGetKey(windowRef, key) == 1;
}

bool isMouseButtonDown(int button)
{
    return glfwGetMouseButton(windowRef, button) == 1;
}

bool isMouseButtonPressed(int button)
{
    return (isMouseButtonDown(button) && !buttons[button]);
}

bool isKeyPressed(int key)
{
    return (isKeyDown(key) && !keys[key]);
}

bool isMouseButtonReleased(int button)
{
    return (!isMouseButtonDown(button) && buttons[button]);
}

bool isKeyReleased(int key)
{
    return (!isKeyDown(key) && keys[key]);
}
} // namespace sle