#ifndef SLE_INPUT_H
#define SLE_INPUT_H

#include <glm/vec2.hpp>

#include "window.hpp"

namespace sle
{
void init(void);
void inputUpdate(void);
glm::vec2 getMousePositionScreen();
bool isKeyDown(int key);
bool isMouseButtonDown(int button);
bool isMouseButtonPressed(int button);
bool isKeyPressed(int key);
bool isMouseButtonReleased(int button);
bool isKeyReleased(int key);
} // namespace sle

#endif