#ifndef SLE_WINDOW_H
#define SLE_WINDOW_H

#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace sle
{

extern GLFWwindow *windowRef;

int createWindow(std::string title, int width, int height, bool vsync, bool fullscreen);
void windowAfterRender(void);
void destroyWindow(void);
bool windowShouldClose(void);
void setVsync(bool vsync);
} // namespace sle

#endif