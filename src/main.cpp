#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "io/window.hpp"
#include "io/input.hpp"
#include "rendering/Shader.hpp"
#include "rendering/Rectangle.hpp"
#include "rendering/Texture.hpp"
#include "rendering/SpriteRenderer.hpp"
#include "util/types.h"
static const int width = 1280;
static const int height = 720;

int main(void)
{
	const std::string vertexSrc = "\
	#version 130 										\n\
	in vec2 vertices;					 				\n\
	in vec2 textures;									\n\
	uniform mat4 mvpMatrix;								\n\
	out vec2 texCoords;									\n\
	void main(void) { 									\n\
		texCoords = textures;							\n\
		gl_Position = mvpMatrix * vec4(vertices, 0, 1);	\n\
	}";

	const std::string fragmentSrc = "\
	#version 130										\n\
	uniform sampler2D sampler;							\n\
	uniform int textured;								\n\
	in vec2 texCoords;									\n\
	out vec4 color;										\n\
	void main(void) { 									\n\
		if(textured == 1){								\n\
			color = texture2D(sampler, texCoords);		\n\
		}else {											\n\
			color = vec4(0,1,0,1);						\n\
		}												\n\
	}";

	sle::createWindow("Test", 1280, 720, false, false);

	std::cout
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	sle::Shader shader{};
	shader.vertexSource(vertexSrc);
	shader.fragmentSource(fragmentSrc);
	shader.linkShader();
	shader.setAttribLocation("vertices", 0);
	shader.setAttribLocation("textures", 1);

	sle::Rectangle rectangle{true, true};

	sle::Texture texture{"resources/images/tile2.png"};

	glm::mat4 projection{glm::ortho(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f)};
	glm::mat4 view{glm::identity<glm::mat4>()};
	view = glm::scale(view, glm::vec3{32});
	//glViewport(0, 0, 1280, 720);
	glClearColor(0, 0, 0, 1.0f);

	while (!sle::windowShouldClose())
	{

		glm::mat4 vp{view * projection};

		if (sle::isKeyPressed(GLFW_KEY_SPACE))
		{
			rectangle.setWireframe(!rectangle.isWireframe());
		}

		shader.bind();
		shader.setUniform("textured", rectangle.isTextured() ? 1 : 0);
		shader.setUniform("sampler", 0);
		shader.setUniform("mvpMatrix", vp);

		texture.bindToSampler(0);
		rectangle.render();

		shader.unbind();

		GLenum err{glGetError()};
		if (err != GL_NO_ERROR)
		{
			std::cerr << err << std::endl;
		}

		sle::inputUpdate();
		sle::windowAfterRender();
	}

	sle::destroyWindow();

	return 0;
}
