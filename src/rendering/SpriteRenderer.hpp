#ifndef SLE_SPRITE_RENDERER_H
#define SLE_SPRITE_RENDERER_H

#include <memory>

#include "../model/SimpleComponent.hpp"
#include "Texture.hpp"
#include "Rectangle.hpp"
#include "Shader.hpp"

namespace sle
{
class SpriteRenderer : public SimpleComponent
{
private:
    Rectangle model;
    Texture sprite;
    std::shared_ptr<Shader> shader;

public:
    SpriteRenderer(std::shared_ptr<SimpleObject> parrent, Texture sptite);
    SpriteRenderer(std::shared_ptr<SimpleObject> parrent, Texture sptite, std::string shaderName);
    ~SpriteRenderer();
    void render(void);
    virtual void update(void);
    std::shared_ptr<Rectangle> getModel();
};
} // namespace sle
#endif