#ifndef SLE_RECTANGLE_H
#define SLE_RECTANGLE_H

#include <GL/glew.h>
#include <iostream>

namespace sle
{
class Rectangle
{
private:
    unsigned int vaoId;
    unsigned int verticesId;
    unsigned int textureCoordsId;
    bool textured;
    bool wireframe;
    static const float vertices[];
    static const float textures[];
    static const float wireframeVertices[];

public:
    Rectangle(bool textured, bool wireframe);
    ~Rectangle();
    void render(void);
    bool isTextured();
    void setWireframe(bool wireframe);
    bool isWireframe();
};

} // namespace sle

#endif