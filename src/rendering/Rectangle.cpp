#include "Rectangle.hpp"

namespace sle
{

const float Rectangle::vertices[] = {
    1, 1,
    -1, 1,
    -1, -1,

    1, 1,
    -1, -1,
    1, -1};
const float Rectangle::textures[] = {
    1, 0,
    0, 0,
    0, 1,

    1, 0,
    0, 1,
    1, 1};

const float Rectangle::wireframeVertices[] = {
    1, 1,
    1, -1,
    -1, -1,
    -1, 1,
    1, 1};

Rectangle::Rectangle(bool textured, bool wireframe) : textured{textured},
                                                      wireframe{wireframe}, verticesId{0}, textureCoordsId{0}
{
    glGenVertexArrays(1, &this->vaoId);
    glBindVertexArray(this->vaoId);

    glGenBuffers(1, &this->verticesId);
    Rectangle::setWireframe(wireframe);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    if (textured)
    {
        glGenBuffers(1, &this->textureCoordsId);
        glBindBuffer(GL_ARRAY_BUFFER, this->textureCoordsId);
        //std::cout << "" << std::endl;
        glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle::textures), Rectangle::textures, GL_STATIC_DRAW);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Rectangle::~Rectangle()
{
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

    glDeleteBuffers(1, &this->verticesId);
    if (this->textured)
    {
        glDeleteBuffers(1, &this->textureCoordsId);
    }
    glDeleteVertexArrays(1, &this->vaoId);
}

void Rectangle::render(void)
{
    glBindVertexArray(this->vaoId);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    //glDrawElements(, 6, GL_UNSIGNED_INT, nullptr);
    glDrawArrays(this->wireframe ? GL_LINE_STRIP : GL_TRIANGLES, 0, this->wireframe ? 5 : 6);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
}

bool Rectangle::isTextured(void)
{
    return this->textured;
}
void Rectangle::setWireframe(bool wireframe)
{
    this->wireframe = wireframe;
    glBindBuffer(GL_ARRAY_BUFFER, this->verticesId);
    glBufferData(GL_ARRAY_BUFFER, wireframe ? sizeof(Rectangle::wireframeVertices) : sizeof(Rectangle::vertices), wireframe ? Rectangle::wireframeVertices : Rectangle::vertices, GL_STATIC_DRAW);
}
bool Rectangle::isWireframe(void)
{
    return this->wireframe;
}

} // namespace sle