#ifndef SLE_TEXTURE_H
#define SLE_TEXTURE_H

#include <GL/glew.h>
#include <string>
#include "../stb/stb_image.h"

namespace sle
{
class Texture
{
private:
    unsigned int textureId;

public:
    int width;
    int height;

    Texture(std::string imageFile);
    ~Texture();
    void bindToSampler(int sampler);
};
} // namespace sle

#endif