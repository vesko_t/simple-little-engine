#ifndef SLE_SHADER_H
#define SLE_SHADER_H

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <GL/glew.h>
#include <iostream>

namespace sle
{
class Shader
{
private:
    int programId;
    int vertexId;
    int fragmentId;
    int geometryId;

public:
    Shader();
    ~Shader();

    void vertexSource(std::string source);

    void fragmentSource(std::string source);

    void geometrySource(std::string source);

    void linkShader();

    void bind();

    void unbind();

    void setAttribLocation(std::string attribName, int index);

    void setUniform(std::string uniformName, int value);

    void setUniform(std::string uniformName, float value);

    void setUniform(std::string uniformName, glm::vec4);

    void setUniform(std::string uniformName, glm::vec3);

    void setUniform(std::string uniformName, glm::vec2);

    void setUniform(std::string uniformName, glm::mat4);
};
} // namespace sle
#endif