#include "Shader.hpp"
namespace sle
{
Shader::Shader() : programId{-1}, vertexId{-1}, fragmentId{-1}, geometryId{-1}
{
    this->programId = glCreateProgram();
};
Shader::~Shader()
{
    glUseProgram(0);
    if (this->geometryId != -1)
    {
        glDetachShader(this->programId, this->geometryId);
        glDeleteShader(this->geometryId);
    }
    if (this->fragmentId != -1)
    {
        glDetachShader(this->programId, this->fragmentId);
        glDeleteShader(this->fragmentId);
    }
    if (this->vertexId != -1)
    {
        glDetachShader(this->programId, this->vertexId);
        glDeleteShader(this->vertexId);
    }
    glDeleteProgram(this->programId);
}

void Shader::vertexSource(std::string source)
{
    this->vertexId = glCreateShader(GL_VERTEX_SHADER);
    const char *sourceChar = source.c_str();
    glShaderSource(this->vertexId, 1, &sourceChar, nullptr);
    glCompileShader(this->vertexId);
    int isCompiled;
    glGetShaderiv(this->vertexId, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        char infoLog[512];
        int infoLength;
        glGetShaderiv(this->vertexId, GL_INFO_LOG_LENGTH, &infoLength);
        glGetShaderInfoLog(this->vertexId, 512, nullptr, infoLog);
        std::cerr << "Vertex error\n";
        for (int i = 0; i < strlen(infoLog); i++)
        {
            std::cerr << infoLog[i];
        }
        std::cerr << std::endl;
        glDeleteShader(this->vertexId);
        glDeleteProgram(this->programId);

        return;
    }
}

void Shader::fragmentSource(std::string source)
{
    this->fragmentId = glCreateShader(GL_FRAGMENT_SHADER);

    const char *sourceChar = source.c_str();
    glShaderSource(this->fragmentId, 1, &sourceChar, NULL);
    glCompileShader(this->fragmentId);
    int isCompiled{0};
    glGetShaderiv(this->fragmentId, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        char infoLog[512];

        glGetShaderInfoLog(this->fragmentId, 512, nullptr, infoLog);
        std::cerr << "Fragment error\n";
        for (int i = 0; i < strlen(infoLog); i++)
        {
            std::cerr << infoLog[i];
        }
        std::cerr << std::endl;

        glDeleteShader(this->fragmentId);
        glDeleteProgram(this->programId);

        return;
    }
}

void Shader::geometrySource(std::string source)
{
    this->geometryId = glCreateShader(GL_GEOMETRY_SHADER);

    const char *sourceChar = source.c_str();
    glShaderSource(this->geometryId, 1, &sourceChar, NULL);
    glCompileShader(this->geometryId);
    int isCompiled{0};
    glGetShaderiv(this->geometryId, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        char infoLog[512];

        glGetShaderInfoLog(this->geometryId, 512, nullptr, infoLog);
        std::cerr << "Geometry error\n";
        for (int i = 0; i < strlen(infoLog); i++)
        {
            std::cerr << infoLog[i];
        }
        std::cerr << std::endl;
        glDeleteShader(this->geometryId);
        glDeleteProgram(this->programId);

        return;
    }
}

void Shader::linkShader()
{
    if (this->vertexId != -1)
    {
        glAttachShader(this->programId, this->vertexId);
    }
    if (this->fragmentId != -1)
    {
        glAttachShader(this->programId, this->fragmentId);
    }
    if (this->geometryId != -1)
    {
        glAttachShader(this->programId, this->geometryId);
    }
    glLinkProgram(this->programId);
    int isCompiled{0};
    glGetProgramiv(this->programId, GL_LINK_STATUS, &isCompiled);
    if (!isCompiled)
    {
        char infoLog[512];
        glGetProgramInfoLog(this->programId, 512, nullptr, infoLog);
        std::cerr << "Link error\n";
        for (int i = 0; i < strlen(infoLog); i++)
        {
            std::cerr << infoLog[i];
        }
        std::cerr << std::endl;
        glDeleteProgram(this->programId);
    }
    glValidateProgram(this->programId);
}

void Shader::setAttribLocation(std::string attribName, int index)
{
    glBindAttribLocation(this->programId, index, attribName.c_str());
}

void Shader::bind()
{
    glUseProgram(programId);
}

void Shader::unbind()
{
    glUseProgram(0);
}
void Shader::setUniform(std::string uniformName, int value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
        glUniform1i(location, value);
}

void Shader::setUniform(std::string uniformName, float value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
        glUniform1f(location, value);
}

void Shader::setUniform(std::string uniformName, glm::vec3 value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
    {
        glUniform3f(location, value.x, value.y, value.z);
    }
}

void Shader::setUniform(std::string uniformName, glm::vec4 value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
    {
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }
}

void Shader::setUniform(std::string uniformName, glm::vec2 value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
    {
        glUniform2f(location, value.x, value.y);
    }
}

void Shader::setUniform(std::string uniformName, glm::mat4 value)
{
    int location = glGetUniformLocation(this->programId, uniformName.c_str());
    if (location != -1)
    {
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
    }
}
} // namespace sle