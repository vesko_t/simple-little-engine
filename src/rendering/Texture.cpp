#define STB_IMAGE_IMPLEMENTATION
#include "Texture.hpp"

namespace sle
{

Texture::Texture(std::string imageFile) : textureId{0}, width{0}, height{0}
{
    unsigned char *texture{nullptr};

    texture = stbi_load(imageFile.c_str(), &this->width, &this->height, nullptr, STBI_rgb_alpha);

    glGenTextures(1, &this->textureId);

    glBindTexture(GL_TEXTURE_2D, this->textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(texture);
}
Texture::~Texture()
{
    glDeleteTextures(1, &this->textureId);
}
void Texture::bindToSampler(int sampler)
{
    glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + sampler));
    glBindTexture(GL_TEXTURE_2D, this->textureId);
}

} // namespace sle