#include "SpriteRenderer.hpp"
#include "../util/shaders.hpp"
class SimpleComponent;

namespace sle
{

SpriteRenderer::SpriteRenderer(std::shared_ptr<SimpleObject> parrent, Texture sptite)
    : SpriteRenderer{parrent, sptite, "standard"}
{
}

SpriteRenderer::SpriteRenderer(std::shared_ptr<SimpleObject> parrent, Texture sptite, std::string shaderName)
    : SimpleComponent(parrent),
      sprite{sprite},
      model{true, false},
      shader{getShader(shaderName)}
{
}

SpriteRenderer::~SpriteRenderer()
{
}
void SpriteRenderer::render(void)
{
    shader->bind();
    shader->setUniform("textured", 1);
    shader->setUniform("sampler", 0);
    sprite.bindToSampler(0);
    model.render();
}

void SpriteRenderer::update(void)
{
}
} // namespace sle