#ifndef SLE_ORIENTEDBOUNDING_BOX_H
#define SLE_ORIENTEDBOUNDING_BOX_H

#include <glm/vec2.hpp>
#include <memory>

namespace sle
{
class OrientedBoundingBox
{

public:
    std::shared_ptr<glm::vec2> test;
    std::shared_ptr<glm::vec2> scale;
    std::shared_ptr<glm::vec2> position;
    std::shared_ptr<glm::vec2> topRight;
    std::shared_ptr<glm::vec2> bottomRight;
    std::shared_ptr<glm::vec2> topLeft;
    std::shared_ptr<glm::vec2> bottomLeft;
    std::shared_ptr<glm::vec2> verticalNormal;
    std::shared_ptr<glm::vec2> horizontalNormal;

    void update(void);

    void rotateVertices(float angleRadians);

    void calculateEdgeNormals();
};
} // namespace sle

#endif