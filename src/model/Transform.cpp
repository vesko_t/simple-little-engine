#include "Transform.hpp"
#include <iostream>
namespace sle
{

Transform::Transform() : Transform(glm::vec2{0})
{
}

Transform::Transform(glm::vec2 position)
    : Transform(position, glm::vec2{1})
{
}

Transform::Transform(glm::vec2 position, glm::vec2 scale)
    : position{position}, scale{scale}, transform{glm::identity<glm::mat4>()}
{
}

Transform::~Transform()
{
    std::cout << "Kill transform" << std::endl;
}

} // namespace sle