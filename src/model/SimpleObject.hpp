#ifndef SLE_SIMPLE_OBJECT_H
#define SLE_SIMPLE_OBJECT_H

#include "Transform.hpp"
#include <memory>
#include <vector>

namespace sle
{
class SimpleComponent;
class SimpleObject
{
private:
    std::shared_ptr<Transform> transform;
    std::shared_ptr<Transform> localTransform;

public:
    std::shared_ptr<SimpleObject> parrent;
    std::shared_ptr<std::vector<SimpleComponent>> components;
    std::shared_ptr<std::vector<std::shared_ptr<SimpleObject>>> children;

    SimpleObject();
    SimpleObject(glm::vec2 position);
    SimpleObject(glm::vec2 position, glm::vec2 scale);
    virtual void update(void);
    std::shared_ptr<Transform> getLocalTransform();
    std::shared_ptr<Transform> getTransform();
};
} // namespace sle
#endif