#ifndef SLE_TRANSFORM_H
#define SLE_TRANSFORM_H

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/vec2.hpp>
#include <glm/gtx/quaternion.hpp>

namespace sle
{
class Transform
{
private:
    /* data */
public:
    glm::vec2 position;
    glm::vec2 scale;
    glm::quat rotation;
    glm::mat4 transform;

    Transform();

    Transform(glm::vec2 position);

    Transform(glm::vec2 position, glm::vec2 scale);

    ~Transform();
};
} // namespace sle

#endif