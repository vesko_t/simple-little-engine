#include "SimpleObject.hpp"

#include <iostream>

namespace sle
{

SimpleObject::SimpleObject() : SimpleObject(glm::vec2{0})
{
}

SimpleObject::SimpleObject(glm::vec2 position) : SimpleObject(position, glm::vec2{1})
{
}

SimpleObject::SimpleObject(glm::vec2 position, glm::vec2 scale)
{
    std::make_shared<Transform>(position, scale);
}

void SimpleObject::update()
{
}

} // namespace sle