#ifndef SLE_SIMPLE_COMPONENT_H
#define SLE_SIMPLE_COMPONENT_H

#include <memory>

#include "SimpleObject.hpp"
namespace sle
{
class SimpleComponent
{
protected:
    std::shared_ptr<SimpleObject> parrent;
    SimpleComponent();

public:
    SimpleComponent(std::shared_ptr<SimpleObject> parrent);
    virtual void update() = 0;
};
} // namespace sle
#endif