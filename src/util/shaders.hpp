#ifndef SLE_SHADERS_H
#define SLE_SHADERS_H

#include <memory>
#include <vector>
#include <map>
#include <string>

#include "../rendering/Shader.hpp"

namespace sle
{

void addShader(std::shared_ptr<Shader> shader, std::string shaderName);

std::shared_ptr<Shader> getShader(std::string shaderName);

} // namespace sle

#endif