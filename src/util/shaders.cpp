#include "shaders.hpp"

namespace sle
{
std::map<std::string, std::shared_ptr<Shader>> shaders{};

void addShader(std::shared_ptr<Shader> shader, std::string shaderName)
{
    shaders[shaderName] = shader;
}

std::shared_ptr<Shader> getShader(std::string shaderName)
{
    return shaders.find(shaderName)->second;
}
} // namespace sle